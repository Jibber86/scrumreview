<?php
//require
require 'database/Connection.php';
require 'models/UserModel.php';
//Connection class
$conn = Connection::make();

$data = new UserModel($conn);
$users = $data->allUsers();

//view
require 'views/index.view.php';




















/**
 * Created by: Stephan Hoeksema 2022
 * ScrumReview
 * - Installeer git
 *   - check versie git op de command-line/terminal (git --version)
 *
 * - Installeer MariaDB/MySQL
 *   - check versie MarieDB/MySQL op de command-line/terminal door in te loggen
 *     (mysql -u<jouw_username> -p<jouw_password>)
 *   - Maak op de command-line/terminal wanneer je ingelogd bent,
 *     de volgende database aan:
 *     (create database scrumReview;)
 */

/******************************************
 * Zorg voor een view van de landing page.
 * views/index.view.php - Start
 ******************************************
 * Zorg voor een database connectie class.
 * - Tools -> database
 * - Druk op + symbool kies datasource
 * - Gegevens voor mysql/mariadb invoeren
 *
 ******************************************
 * Zorg voor een database connectie door
 * middel van een class:
 * - Maak de directory 'database'
 * - Maak een nieuwe php class Connection.php in
 *   deze directory
 * - Maak variabele voor de gegevens:
 *   - host
 *   - dbname
 *   - user
 *   - password
 * - Maak een try...catch... block
 *   - Probeer(try) een PDO connectie te maken
 *   - Vang(catch) de error messages
 * - Test het door een nieuwe connectie te maken in de index.php
 *   - require 'database/Connection.php'
 *   - $conn = Connection::make();
 *   - die(var_dump($conn));
 * - Gebruikers laten zien en registreer formulier
 *   - $users = $pdo->allUsers()
 *   - Users in de view verwerken met een foreach()
 * */

